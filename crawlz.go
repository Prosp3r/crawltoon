// http_request_change_headers.go
package main

import (
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"

	//"net/url"
	"os"
	"regexp"
	"strings"

	"github.com/PuerkitoBio/goquery"
)

func main() {
	// Create HTTP client with timeout
	//client := &http.Client{
	//	Timeout: 30 * time.Second,
	//}

	//url := "https://www.devdungeon.com"
	//url := "https://www.porncomixonline.net/blacknwhite-the-mayor-4/"
	url := "https://www.porncomixonline.net/blacknwhite-i-hate-my-mother/"

	siteName := "pcomics"
	// Create and modify HTTP request before sending
	GetImagesOnPage(url, siteName)

	/*request, err := http.NewRequest("GET", url, nil)
	if err != nil {
		log.Fatal(err)
	}
	request.Header.Set("User-Agent", "Not Firefox")

	// Make request
	response, err := client.Do(request)
	if err != nil {
		log.Fatal(err)
	}
	defer response.Body.Close()

	// Copy data from the response to standard output
	_, err = io.Copy(os.Stdout, response.Body)
	if err != nil {
		log.Fatal(err)
	}*/
}

//
func GetImagesOnPage(url, siteName string) {
	// Make HTTP request
	//response, err := http.Get("https://www.devdungeon.com")
	response, err := http.Get(url)
	if err != nil {
		log.Fatal(err)
	}
	defer response.Body.Close()

	// Create a goquery document from the HTTP response
	document, err := goquery.NewDocumentFromReader(response.Body)
	if err != nil {
		//fmt.Println(err.Error())
		log.Fatal("Error loading HTTP response body. ", err)
	}

	// Find and print image URLs
	document.Find("img").Each(func(index int, element *goquery.Selection) {
		imgSrc, exists := element.Attr("src")
		if exists {
			//write to file payload
			//GetImageContent(imgSrc, siteName)
			fmt.Println(imgSrc)
		}
	})

	// Find and print image URLs
	document.Find("a").Each(func(index int, element *goquery.Selection) {
		imgSrc, exists := element.Attr("href")
		if exists {
			//write to file payload
			//GetImageContent(imgSrc, siteName)
			isAPic := isPicture(imgSrc)
			if isAPic == true {
				GetImageContent(imgSrc, siteName)
				fmt.Println(imgSrc)
			}

		}
	})
}

//IsPic
func isPicture(imgSrc string) bool {
	fmt.Printf("Long ==> %v", len(imgSrc))

	if len(imgSrc) > 4 {
		urlx := strings.Split(imgSrc, "/")
		urlxCount := len(urlx)
		imgN := urlx[urlxCount-1]
		if len(imgN) > 2 {
			imgExts := strings.Split(imgN, ".")
			imgExt := imgExts[1]
			if imgExt == "jpg" || imgExt == "JPG" || imgExt == "png" || imgExt == "PNG" {
				fmt.Println("IS-AN-IMAGE")
				return true
			}
		}
	}
	return false
}

func GetImageContent(url, siteName string) {
	// Make request
	//response, err := http.Get("https://www.devdungeon.com/archive")
	urlx := strings.Split(url, "/")
	urlxCount := len(urlx)
	imgN := urlx[urlxCount-1]
	response, err := http.Get(url)
	if err != nil {
		fmt.Println(err.Error())
		//log.Fatal(err)
	}
	defer response.Body.Close()

	// Create output file
	fmt.Println(url)
	fmt.Println(imgN)
	outFile, err := os.Create("./payload/Mayor4/" + imgN)
	if err != nil {
		fmt.Println(err.Error())
		//log.Fatal(err)
	}
	defer outFile.Close()

	// Copy data from HTTP response to file
	_, err = io.Copy(outFile, response.Body)
	if err != nil {
		fmt.Println(err.Error())
		//log.Fatal(err)
	}
}

//
func GetContent(url, siteName string) {
	// Make request
	//response, err := http.Get("https://www.devdungeon.com/archive")
	response, err := http.Get(url)
	if err != nil {
		log.Fatal(err)
	}
	defer response.Body.Close()

	// Create output file
	outFile, err := os.Create("payload/" + siteName + "/output.html")
	if err != nil {
		log.Fatal(err)
	}
	defer outFile.Close()

	// Copy data from HTTP response to file
	_, err = io.Copy(outFile, response.Body)
	if err != nil {
		log.Fatal(err)
	}
}

func GetPageTitle(url string) {
	// Make HTTP GET request
	//response, err := http.Get("https://www.devdungeon.com/")
	response, err := http.Get(url)
	if err != nil {
		log.Fatal(err)
	}
	defer response.Body.Close()

	// Get the response body as a string
	dataInBytes, err := ioutil.ReadAll(response.Body)
	pageContent := string(dataInBytes)

	// Find a substr
	titleStartIndex := strings.Index(pageContent, "<title>")
	if titleStartIndex == -1 {
		fmt.Println("No title element found")
		os.Exit(0)
	}
	// The start index of the title is the index of the first
	// character, the < symbol. We don't want to include
	// <title> as part of the final value, so let's offset
	// the index by the number of characers in <title>
	titleStartIndex += 7

	// Find the index of the closing tag
	titleEndIndex := strings.Index(pageContent, "</title>")
	if titleEndIndex == -1 {
		fmt.Println("No closing tag for title found.")
		os.Exit(0)
	}

	// (Optional)
	// Copy the substring in to a separate variable so the
	// variables with the full document data can be garbage collected
	pageTitle := []byte(pageContent[titleStartIndex:titleEndIndex])

	// Print out the result
	fmt.Printf("Page title: %s\n", pageTitle)
}

func GetComments(url string) {
	// Make HTTP request
	//response, err := http.Get("https://www.devdungeon.com")
	response, err := http.Get(url)
	if err != nil {
		log.Fatal(err)
	}
	defer response.Body.Close()

	// Read response data in to memory
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		log.Fatal("Error reading HTTP body. ", err)
	}

	// Create a regular expression to find comments
	re := regexp.MustCompile("<!--(.|\n)*?-->")
	comments := re.FindAllString(string(body), -1)
	if comments == nil {
		fmt.Println("No matches.")
	} else {
		for _, comment := range comments {
			fmt.Println(comment)
		}
	}
}

// This will get called for each HTML element found
func processElement(index int, element *goquery.Selection) {
	// See if the href attribute exists on the element
	href, exists := element.Attr("href")
	if exists {
		fmt.Println(href)
	}
}

//
func GetLinksinPage(url string) {
	// Make HTTP request
	//response, err := http.Get("https://www.devdungeon.com")
	response, err := http.Get(url)
	if err != nil {
		log.Fatal(err)
	}
	defer response.Body.Close()

	// Create a goquery document from the HTTP response
	document, err := goquery.NewDocumentFromReader(response.Body)
	if err != nil {
		log.Fatal("Error loading HTTP response body. ", err)
	}

	// Find all links and process them with the function
	// defined earlier
	document.Find("a").Each(processElement)
}

//
/*
func ParseUrls(url string) {
	// Parse a complex URL
	//complexUrl := "https://www.example.com/path/to/?query=123&this=that#fragment"
	complexURL := url
	parsedURL, err := url.Parse(complexURL)
	if err != nil {
		log.Fatal(err)
	}

	// Print out URL pieces
	fmt.Println("Scheme: " + parsedUrl.Scheme)
	fmt.Println("Host: " + parsedUrl.Host)
	fmt.Println("Path: " + parsedUrl.Path)
	fmt.Println("Query string: " + parsedUrl.RawQuery)
	fmt.Println("Fragment: " + parsedUrl.Fragment)

	// Get the query key/values as a map
	fmt.Println("\nQuery values:")
	queryMap := parsedUrl.Query()
	fmt.Println(queryMap)

	// Craft a new URL from scratch
	var customURL url.URL
	customURL.Scheme = "https"
	customURL.Host = "google.com"
	newQueryValues := customURL.Query()
	newQueryValues.Set("key1", "value1")
	newQueryValues.Set("key2", "value2")
	customURL.Fragment = "bookmarkLink"
	customURL.RawQuery = newQueryValues.Encode()

	fmt.Println("\nCustom URL:")
	fmt.Println(customURL.String())

}
*/
//url := "https://www.porncomixonline.net/blacknwhite-night-shift-at-beaverton-general/"
//url := "https://www.porncomixonline.net/blacknwhite-housewives-of-beaverton/"
//url := "https://www.porncomixonline.net/poker-game-2-blacknwhite/"
//url := "https://www.porncomixonline.net/poker-game/"
//url := "https://www.porncomixonline.net/blacknwhite-mayor-3/"
//url := "https://www.porncomixonline.net/blacknwhite-mayor-2/"
//url := "https://www.porncomixonline.net/blacknwhite-the-mayor-4/"
